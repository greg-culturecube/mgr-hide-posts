<?php
/**
 *Plugin Name: MGR Hide Posts
 *Description: Hide specific post from the MGR blog archive page
 *Author: Greg Lorenzen
 *Version: 1.0.0
**/

class MGRHidePosts {

    public function __construct() {
        add_action('pre_get_posts', array($this, 'hide_post_on_archive'));
    }

    public function hide_post_on_archive($query) {
        // var_dump($query);

        if($query->is_archive() || $query->is_home() && !$query->is_admin() && $query->is_main_query()) {
            // hide post with 'id' of '1'
            $query->set('post__not_in', array('1595'));
        }
    }

}

new MGRHidePosts();